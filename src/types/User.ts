export default interface Product {
  id?: number;
  username: string;
  password: string;
  createAt?: Date;
  updatedAt?: Date;
  deleteAt?: Date;
}
